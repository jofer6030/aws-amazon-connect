aws cloudformation create-stack --stack-name test-amazon-connect --template-body .\aws-amazon-connect\stack.yml --profile user-terraform --region us-east-1


aws connect describe-contact-flow --instance-id c30d826d-5fc9-4b2d-80ea-6c3d56d687c8 --contact-flow-id ffd2e8a8-c845-43bf-aef5-429e6ff7eba2 --region us-east-1 --profile user-terraform | jq '.ContactFlow.Content | fromjson' > flow-aws-cli.json