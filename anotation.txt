default =  ["SCHEDULED_REPORTS","CALL_RECORDINGS","CHAT_TRANSCRIPTS"]
Wilmer Segura17:16
phone_config {
    after_contact_work_time_limit = 0
    phone_type                    = "SOFT_PHONE"
  }
Wilmer Segura18:05
data "aws_connect_routing_profile" "routing" {
  instance_id = aws_connect_instance.instancia_connect.id
  name = "Basic Routing Profile"
}

data "aws_connect_security_profile" "security" {
  instance_id = aws_connect_instance.instancia_connect.id
  name        = "Admin"
}